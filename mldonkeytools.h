/* mldonkey tools: Quick'n Dirty Lib to access mldonkey core.
 * Copyright (c) 2004 Christophe Badoit
 *
 * Original Author: Christophe Badoit <tof-at-tof2k-dot-com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/* Quick'n Dirty Lib to access mldonkey core.
 * Made for Gkremldk, a gkrellm plugin.
 *
 * Please send me feedback if you ever use it :-)
 *
 * Based on 
 * - http://www.g2gui.net/wiki/index.php/GuiProtocol 
 * - http://royale.zerezo.com/jmoule/protocol.php
 * - and tcpdump ! :-) 
 *
 */

#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 

#define MAX_MSG_LEN 1024
//#define DUMP_BUF(a,b,c) dumpBuf(a,b,c)
#define DUMP_BUF(a,b,c)


typedef struct _donkeyMsg {
	long size;
	int pos;
	short int opcode;
	char *payload;
} donkeyMsg;


//debug, dump a buffer of length len, prefixed by msg
void dumpBuf(char *msg, char *buf, int len);

// assert we can read/write ahead n chars in the msg 
int canAdvance(donkeyMsg *m, int n);

//Write functions
void writeByte(donkeyMsg *m, unsigned char c);

void writeInt(donkeyMsg *m, unsigned short int i);

void writeLong(donkeyMsg *m, unsigned int l);

void writeLong64(donkeyMsg *m, unsigned long l);

void writeString(donkeyMsg *m, char *string);

// Send a donkey message into the socket.
int sendMsg(int sockfd, donkeyMsg *m);

/* Read functions */
unsigned char readByte(donkeyMsg *m);

unsigned short int readInt(donkeyMsg *m);

unsigned int readLong(donkeyMsg *m);

unsigned long readLong64(donkeyMsg *m);

char* readString(donkeyMsg *m);

/* Read a donkey message from a socket */
int readMsg(int sockfd, donkeyMsg *m);

/* Prepare a message for sending, giving the opcode */
void prepareMsg(donkeyMsg *m, short int opcode);

/* Free payload from a message */
void freeMsg(donkeyMsg *m);

/* Tempt to connect to a core. return 1 on success, 0 on failure */
int donkeyConnect(int *sockfd, char *host,
	int portno, char *login, char *password);


