/* vim:ts=4:sw=4:
 *
 * GKrellmdk: GKrellM MLDonkey Plugin
 * Copyright (c) 2004 Christophe Badoit
 *
 * Original Author: Christophe Badoit <tof-at-tof2k-dot-com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include <gkrellm2/gkrellm.h>
#include <pthread.h>

/*FIXME: should include the .h, but does not work ?*/
#include "mldonkeytools.c"

#define	CONFIG_NAME	"Gkremldk"
#define MONITOR_CONFIG_KEYWORD  "gkremldk"
#define	STYLE_NAME	"gkremldk"

static GkrellmTicks        *g_ticks;
static GkrellmMonitor      *monitor;
static GkrellmPanel        *panel_krells;
static GkrellmPanel        *panel_cmd;
static GkrellmDecal        *dl_decal;
static GkrellmDecal        *ul_decal;
static GkrellmKrell        *dl_krell;
static GkrellmKrell        *ul_krell;
static GkrellmKrell        *dl_krell_max;
static GkrellmKrell        *ul_krell_max;
static GtkTooltips         *panel_tooltip = NULL;
static gint                style_id;

static GkrellmChart        *chart;
static GkrellmChartconfig  *chart_config;
static GkrellmChartdata    *dl_chart_data;
static GkrellmChartdata    *ul_chart_data;

static GkrellmDecal        *start_stop_decal;

static gfloat              dl_rate = 0;
static gfloat              ul_rate = 0;

static gfloat              dl_rate_max = 15.0;
static gfloat              ul_rate_max = 15.0;

static gchar               tooltip_text[1024];

/*status flags*/
static gboolean            isConnected = FALSE;
static gboolean            haveToReconnect = FALSE;
static gboolean            chart_show_rates = TRUE;
static gboolean            chart_isvisible = TRUE;
static gint                display_mode = 0; // 0=krells, 1=chart, 2=cmds*/

/*thread, connecting to mldonkey and getting rates*/
pthread_t			       donkey_thread;

/*Socket for mldonkey*/
int                        sockfd;

/*Configurations values*/
static gfloat              dl_bandwith_max = 64.0;
static gfloat              ul_bandwith_max = 16.0;
static gfloat              dl_rate_max_paused = 1;
static gfloat              ul_rate_max_paused = 1;

static gchar               *login = "admin";
static gchar               *password = "admin";
static gchar               *host = "localhost";
static gint                port = 4001;

static gchar               *gui_command = "mlgui";
static gchar               *start_core_command = "/etc/init.d/mldonkey-server start";
static gchar               *show_directory_command = "nautilus /";

//Configurations Widgets
static GtkWidget	       *dl_bandwith_max_widget;
static GtkWidget	       *ul_bandwith_max_widget;
static GtkWidget	       *dl_rate_max_paused_widget;
static GtkWidget	       *ul_rate_max_paused_widget;

static GtkWidget	       *host_widget;
static GtkWidget	       *port_widget;
static GtkWidget	       *login_widget;
static GtkWidget	       *password_widget;

static GtkWidget	       *gui_command_widget;
static GtkWidget	       *start_core_command_widget;
static GtkWidget	       *show_directory_command_widget;

/*Send a console command to mldonkey core*/
void sendConsoleCommand(gchar *cmd) {
	donkeyMsg msg;

	prepareMsg(&msg, 29);
	writeString(&msg, cmd);
	sendMsg(sockfd, &msg);
	freeMsg(&msg);
}

/*Set the max_hard_download_rate value*/
void setMaxHardDownloadRate(gfloat value) {
	donkeyMsg msg;

	char buf[16];
	sprintf(buf, "%d", (int)value);
	prepareMsg(&msg, 28);
	writeString(&msg, "max_hard_download_rate");
	writeString(&msg, buf);
	sendMsg(sockfd, &msg);
	freeMsg(&msg);
}

/*Set the max_hard_upload_rate value*/
void setMaxHardUploadRate(gfloat value) {
	donkeyMsg msg;

	char buf[16];
	sprintf(buf, "%d", (int)value);
	prepareMsg(&msg, 28);
	writeString(&msg, "max_hard_upload_rate");
	writeString(&msg, buf);
	sendMsg(sockfd, &msg);
	freeMsg(&msg);
}


/*Donkey Thread*/
void *donkey_update(void *arg) {

	donkeyMsg msg;
	gint      downloads = 0;
	gint      downloads_ended = 0;
	gint      nb_shared = 0;
	gfloat    shared_size = 0;

	int n;

	while (1) {

		//Main Loop, try to connect permanently

		close(sockfd);
		isConnected = FALSE;
		gkrellm_draw_decal_text(panel_cmd, start_stop_decal,
			_("Start core"), -1);
		haveToReconnect = FALSE;

/*		printf("Tempting Connection mldonkey://%s:%s@%s:%d\n",*/
/*				login, password, host, port);*/
		if (!donkeyConnect(&sockfd, host, port, login, password)) {
			sleep(3);
			continue;
		}


		while (!haveToReconnect) {

			// Read donkey message

			if ((n = readMsg(sockfd, &msg)) <= 0) {
/*				printf("Oooops: readMsg returned %d, either server is"*/
/*					" disconnected, or login/password are incorrect.\n",n);*/
				sleep(3);
				break;
			}

			// manage message, depending on opcode
			// See http://www.g2gui.net/wiki/index.php/GuiProtocol

			switch(msg.opcode) {
				case 49:
					// clientStats
					shared_size = (readLong(&msg) / 1000000.0); // 1
					readLong(&msg); // 2
					readLong(&msg); // 3
					readLong(&msg); // 4
					readLong(&msg); // 5
					readLong(&msg); // 6
					nb_shared = readLong(&msg); // 7
					ul_rate = (readLong(&msg) / 1000.0); // 8
					dl_rate = (readLong(&msg) / 1000.0); // 9
					readLong(&msg); // 10
					readLong(&msg); // 11
					downloads = readLong(&msg); // 12
					downloads_ended = readLong(&msg); // 13
					//current_downloads = readLong(&msg);  // 10

					sprintf(tooltip_text,
							"%s: %1.3f\n%s: %1.3f\n%s: %d/%d\n"
							"%s: %d(%1.3f %s)",
							_("Download"), dl_rate,
							_("Upload"), ul_rate,
							_("Downloaded"), downloads_ended, downloads,
							_("Shared"), nb_shared, shared_size, _("Mb"));

					// consider we are connected when this kind of msg is received
					isConnected = TRUE;
					gkrellm_draw_decal_text(panel_cmd, start_stop_decal,
						_("Stop core"), -1);
					break;

				case 1:
					// optionsInfos
					{
						short int nb = readInt(&msg);
						short int i;
						for (i = 0; i < nb; i++) {
							char *key = readString(&msg);
							char *value = readString(&msg);
							if (key != NULL && value != NULL) {
								if (strncmp(key, "max_hard_upload_rate", 20) == 0)
									ul_rate_max = atof(value);
								else if (strncmp(key, "max_hard_download_rate", 22) == 0)
									dl_rate_max = atof(value);
							}
							free(key); free(value);
						}
					}
					break;

				default:
					break;
			}
			
			freeMsg(&msg);

		}

	}

}

/*switch between views (display_mode)*/
/* if mode is -1, juste go forward */
/* 0=krells, 1=chart, 2=cmds */
static void switch_to_display_mode(gint mode) {
	if (mode == -1)
		mode = (display_mode+1) % 3;

	switch (mode) {
		case 0: // krells
			gkrellm_panel_show(panel_krells);
			gkrellm_panel_hide(panel_cmd);
			gkrellm_chart_enable_visibility(chart, FALSE, &chart_isvisible);
			display_mode = mode;
			break;
		case 1: // chart
			gkrellm_panel_hide(panel_krells);
			gkrellm_panel_hide(panel_cmd);
			gkrellm_chart_enable_visibility(chart, TRUE, &chart_isvisible);
			display_mode = mode;
			break;
		case 2: // cmds
			gkrellm_panel_hide(panel_krells);
			gkrellm_panel_show(panel_cmd);
			gkrellm_chart_enable_visibility(chart, FALSE, &chart_isvisible);
			display_mode = mode;
			break;
		default: // does nothing
			break;
	}
}

/*a command button has been clicked*/
static void cmd_button_clicked(GkrellmDecalbutton *button, gpointer data) {

	switch (GPOINTER_TO_INT(data)) {
		case 0: // COMMIT
			sendConsoleCommand("commit");
			break;
		case 1: // START/STOP
			if (isConnected) {
				sendConsoleCommand("kill");
			} else {
				if (strlen(start_core_command) != 0)
					system(start_core_command);
			}
			break;
		case 2: // GUI
			if (strlen(gui_command) != 0)
				system(gui_command);
			break;
		case 3: // DIRECTORY
			if (strlen(show_directory_command) != 0)
				system(show_directory_command);
			break;
		default:
			break;
	}
}

/*Button is pressed on the panel_cmd*/
static void panel_cmd_button_press(GtkWidget *widget, GdkEvent *event) {
	GdkEventButton *ev;
	ev = (GdkEventButton *)event;
    if (ev->button == 2)
		switch_to_display_mode(-1);
	else if (ev->button == 3)
        gkrellm_open_config_window(monitor);
}

/*Button is pressed on the panel_krells*/
static void panel_krells_button_press(GtkWidget *widget, GdkEvent *event) {

	gint x;
	gfloat value;
	GkrellmKrell *krell;
	void (*setValue)(gfloat);
	GdkEventButton *ev;

	ev = (GdkEventButton *)event;

	if (event->type == GDK_SCROLL) {
		GdkEventScroll *scrollEvent = (GdkEventScroll *)event;
		if (scrollEvent->direction == GDK_SCROLL_UP)
			ev->button = 4;
		else if (scrollEvent->direction == GDK_SCROLL_DOWN)
			ev->button = 5;
	}

    if (ev->button ==3 ) {
        gkrellm_open_config_window(monitor);
		return;
	}

	// which krell is clicked ?
	if (ev->y >= dl_decal->y
			&& ev->y <= dl_krell_max->y0 + dl_krell_max->h_frame) {
		krell = dl_krell;
		setValue = &setMaxHardDownloadRate;
		value = dl_rate_max;
	} else if (ev->y >= ul_decal->y
			&& ev->y <= ul_krell_max->y0 + ul_krell_max->h_frame) {
		krell = ul_krell;
		setValue = &setMaxHardUploadRate;
		value = ul_rate_max;
	} else if (ev->button != 2)
		return;

	switch(ev->button) {
		case 1:
			// set values
			x = ev->x * krell->full_scale / (gkrellm_chart_width() - 1);
			if(x < 1) x = 1;
			if(x > 100) x = 100;
			setValue(x);
			break;

		case 2:
			// switch to next display mode
			switch_to_display_mode(-1);
			break;
		case 4:
			if (value < krell->full_scale)
				setValue(value + 1);
			break;
		case 5:
			if (value > 0)
				setValue(value - 1);
			break;
		default:
			return;
			break;
	}

}

/* Update chart */
static void draw_plugin_chart(void) {
	gchar *buf;

	gkrellm_draw_chartdata(chart);

	if (!isConnected) {

		gkrellm_draw_chart_text(chart, style_id, _("\\f\\lConnecting..."));

	} else if (chart_show_rates) {

		buf = g_strdup_printf("\\f\\l%1.1f|%1.1f", dl_rate, ul_rate);
		gkrellm_draw_chart_text(chart, style_id, buf);
		g_free(buf);

	}

	gkrellm_draw_chart_to_screen(chart);
}


/*Button is pressed on the chart*/
static void chart_button_press(GtkWidget *widget, GdkEvent *event) {

	GdkEventButton *ev;

	ev = (GdkEventButton *)event;

	if (event->type == GDK_SCROLL) {
		GdkEventScroll *scrollEvent = (GdkEventScroll *)event;
		if (scrollEvent->direction == GDK_SCROLL_UP)
			ev->button = 4;
		else if (scrollEvent->direction == GDK_SCROLL_DOWN)
			ev->button = 5;
	}

	switch (ev->button) {
		case 1:
			chart_show_rates = !chart_show_rates;
			break;

		case 2:
			switch_to_display_mode(-1);
			break;
		
		case 3:
			gkrellm_chartconfig_window_create(chart);
			break;
		
		default:
			return;
			break;
	}
	
	draw_plugin_chart();
}

/*Update displays*/
static void update_plugin() {

	char *buf;

	if (!isConnected) {
		// show connecting message
		gkrellm_update_krell(panel_krells, dl_krell, 0);
		gkrellm_update_krell(panel_krells, ul_krell, 0);
		gkrellm_update_krell(panel_krells, dl_krell_max, 0);
		gkrellm_update_krell(panel_krells, ul_krell_max, 0);
		gkrellm_draw_decal_text(panel_krells, dl_decal, _("Connecting ..."), -1);
		gkrellm_draw_decal_text(panel_krells, ul_decal, "", -1);

		if (g_ticks->five_second_tick)
			gkrellm_store_chartdata(chart, 0, (gulong)0, (gulong)0);
		draw_plugin_chart();

        gtk_tooltips_set_tip(panel_tooltip, panel_krells->drawing_area, _("Connecting..."), "");
        gtk_tooltips_set_tip(panel_tooltip, panel_cmd->drawing_area, _("Connecting..."), "");
        gtk_tooltips_set_tip(panel_tooltip, chart->drawing_area, _("Connecting..."), "");

	} else {
		// show values
		gkrellm_update_krell(panel_krells, dl_krell, dl_rate);
		gkrellm_update_krell(panel_krells, ul_krell, ul_rate);
		gkrellm_update_krell(panel_krells, dl_krell_max, dl_rate_max);
		gkrellm_update_krell(panel_krells, ul_krell_max, ul_rate_max);

		buf = g_strdup_printf("%1.1f / %d", dl_rate, (int)dl_rate_max);
		gkrellm_draw_decal_text(panel_krells, dl_decal, buf, -1);
		g_free(buf);

		buf = g_strdup_printf("%1.1f / %d", ul_rate, (int)ul_rate_max);
		gkrellm_draw_decal_text(panel_krells, ul_decal, buf, -1);
		g_free(buf);

		if (g_ticks->five_second_tick) {
			gkrellm_store_chartdata(chart, 0,
				(gulong)dl_rate, (gulong)ul_rate);

			if (!chart_show_rates)
				draw_plugin_chart();
		}

		if (chart_show_rates)
			draw_plugin_chart();
	}

	gtk_tooltips_set_tip(panel_tooltip, panel_krells->drawing_area, tooltip_text, "");
	gtk_tooltips_set_tip(panel_tooltip, panel_cmd->drawing_area, tooltip_text, "");
	gtk_tooltips_set_tip(panel_tooltip, chart->drawing_area, tooltip_text, "");

	if (display_mode == 0)
		gkrellm_draw_panel_layers(panel_krells);
	else if (display_mode == 2)
		gkrellm_draw_panel_layers(panel_cmd);
}


static gint panel_cmd_expose_event(GtkWidget *widget, GdkEventExpose *ev) {
	gdk_draw_pixmap(widget->window,
			widget->style->fg_gc[GTK_WIDGET_STATE (widget)],
			panel_cmd->pixmap, ev->area.x, ev->area.y, ev->area.x, ev->area.y,
			ev->area.width, ev->area.height);
	return FALSE;
}

static gint panel_krells_expose_event(GtkWidget *widget, GdkEventExpose *ev) {
    gdk_draw_pixmap(widget->window,
            widget->style->fg_gc[GTK_WIDGET_STATE (widget)],
            panel_krells->pixmap, ev->area.x, ev->area.y, ev->area.x, ev->area.y,
            ev->area.width, ev->area.height);
    return FALSE;
}

static void create_plugin(GtkWidget *vbox, gint first_create) {
	GkrellmStyle        *style;
	GkrellmPiximage     *krell_image;
	GkrellmDecalbutton	*button;
	GkrellmDecal        *decal;
	gint                y, max_height;

	if (first_create) {
		panel_krells = gkrellm_panel_new0();
		panel_cmd = gkrellm_panel_new0();
		chart = gkrellm_chart_new0();
	}


	// Create Krells Panel
	// Create Decal & Krells
	dl_decal = gkrellm_create_decal_text(panel_krells, "0/0",
			gkrellm_meter_textstyle(style_id),
			gkrellm_meter_style(style_id),
			0, 2, -1);
	style = gkrellm_meter_style(style_id);
	gkrellm_set_krell_expand(style, "left");
	krell_image = gkrellm_krell_meter_piximage(style_id);

	dl_krell = gkrellm_create_krell(panel_krells, krell_image, style);
	gkrellm_set_krell_full_scale(dl_krell, dl_bandwith_max, 1);
	gkrellm_move_krell_yoff(panel_krells, dl_krell, dl_decal->y + dl_decal->h + 4);

	dl_krell_max = gkrellm_create_krell(panel_krells, krell_image, style);
	gkrellm_set_krell_full_scale(dl_krell_max, dl_bandwith_max, 1);
	gkrellm_move_krell_yoff(panel_krells, dl_krell_max, dl_krell->y0 + dl_krell->h_frame + 4);


	ul_decal = gkrellm_create_decal_text(panel_krells, "0/0",
			gkrellm_meter_textstyle(style_id),
			gkrellm_meter_style(style_id),
			0, dl_krell_max->y0 + dl_krell_max->h_frame + 4, -1);


	ul_krell = gkrellm_create_krell(panel_krells, krell_image, style);
	gkrellm_set_krell_full_scale(ul_krell, ul_bandwith_max, 1);
	gkrellm_move_krell_yoff(panel_krells, ul_krell, ul_decal->y + ul_decal->h + 4);

	ul_krell_max = gkrellm_create_krell(panel_krells, krell_image, style);
	gkrellm_set_krell_full_scale(ul_krell_max, ul_bandwith_max, 1);
	gkrellm_move_krell_yoff(panel_krells, ul_krell_max, ul_krell->y0 + ul_krell->h_frame + 4);

	gkrellm_monotonic_krell_values(dl_krell, FALSE);
	gkrellm_monotonic_krell_values(ul_krell, FALSE);
	gkrellm_monotonic_krell_values(dl_krell_max, FALSE);
	gkrellm_monotonic_krell_values(ul_krell_max, FALSE);

	gkrellm_panel_configure(panel_krells, NULL, style);


	// Create Commands Panel

	y = 2;

	button = gkrellm_make_scaled_button(panel_cmd, NULL, cmd_button_clicked,
		GINT_TO_POINTER(0), FALSE, FALSE, 0, 0, 0, 2, y, 13, 12);
	decal = gkrellm_create_decal_text(panel_cmd, "Commit",
			gkrellm_meter_textstyle(style_id),
			gkrellm_meter_style(style_id),
			15, y, -1);
	gkrellm_draw_decal_text(panel_cmd, decal, _("Commit"), -1);

	y += 12 + 2;
	button = gkrellm_make_scaled_button(panel_cmd, NULL, cmd_button_clicked,
			GINT_TO_POINTER(1), FALSE, FALSE, 0, 0, 0, 2, y, 13, 12);
	start_stop_decal = gkrellm_create_decal_text(panel_cmd, "Start core",
			gkrellm_meter_textstyle(style_id),
			gkrellm_meter_style(style_id),
			15, y, -1);
	gkrellm_draw_decal_text(panel_cmd, start_stop_decal, _("Start core"), -1);

	y += 12 + 2;
	button = gkrellm_make_scaled_button(panel_cmd, NULL, cmd_button_clicked,
			GINT_TO_POINTER(2), FALSE, FALSE, 0, 0, 0, 2, y, 13, 12);
	decal = gkrellm_create_decal_text(panel_cmd, "GUI",
			gkrellm_meter_textstyle(style_id),
			gkrellm_meter_style(style_id),
			15, y, -1);
	gkrellm_draw_decal_text(panel_cmd, decal, _("GUI"), -1);

	y += 12 + 2;
	button = gkrellm_make_scaled_button(panel_cmd, NULL, cmd_button_clicked,
			GINT_TO_POINTER(3), FALSE, FALSE, 0, 0, 0, 2, y, 13, 12);
	decal = gkrellm_create_decal_text(panel_cmd, "Directoy",
			gkrellm_meter_textstyle(style_id),
			gkrellm_meter_style(style_id),
			15, y, -1);
	gkrellm_draw_decal_text(panel_cmd, decal, _("Directoy"), -1);

	style = gkrellm_meter_style(style_id);
	gkrellm_set_krell_expand(style, "left");

	gkrellm_panel_configure(panel_cmd, NULL, style);



	// Create Chart
	gkrellm_chart_create(vbox, monitor, chart, &chart_config);
	dl_chart_data = gkrellm_add_default_chartdata(chart, _("Download"));
	gkrellm_monotonic_chartdata(dl_chart_data, FALSE);
	ul_chart_data = gkrellm_add_default_chartdata(chart, _("Upload"));
	gkrellm_monotonic_chartdata(ul_chart_data, FALSE);

	gkrellm_set_draw_chart_function(chart, draw_plugin_chart, NULL);

	gkrellm_alloc_chartdata(chart);


	// normalize all heights
	max_height = gkrellm_panel_configure_get_height(panel_cmd);
	if (gkrellm_panel_configure_get_height(panel_krells) > max_height)
		max_height = gkrellm_panel_configure_get_height(panel_krells);
	max_height += 5;
	gkrellm_panel_configure_set_height(panel_krells, max_height);
	gkrellm_panel_configure_set_height(panel_cmd, max_height);
	gkrellm_set_chart_height(chart, max_height);


	gkrellm_panel_create(vbox, monitor, panel_krells);
	gkrellm_panel_create(vbox, monitor, panel_cmd);


	// Create Tooltip
    panel_tooltip = gtk_tooltips_new();
    gtk_tooltips_set_tip(panel_tooltip, panel_krells->drawing_area,
            "", "empty"); /* start with empty string */
    gtk_tooltips_set_tip(panel_tooltip, panel_cmd->drawing_area,
            "", "empty"); /* start with empty string */
    gtk_tooltips_set_tip(panel_tooltip, chart->drawing_area,
            "", "empty"); /* start with empty string */
    gtk_tooltips_set_delay(panel_tooltip, 0);
    gtk_tooltips_enable(panel_tooltip);


	if (first_create) {
	    g_signal_connect(G_OBJECT (panel_krells->drawing_area), "expose_event",
    	        G_CALLBACK(panel_krells_expose_event), NULL);
		gtk_signal_connect(GTK_OBJECT(panel_krells->drawing_area), "button_press_event",
		       (GtkSignalFunc) panel_krells_button_press, NULL);
		gtk_signal_connect(GTK_OBJECT(panel_krells->drawing_area), "scroll-event",
		       (GtkSignalFunc) panel_krells_button_press, NULL);

	    g_signal_connect(G_OBJECT (panel_cmd->drawing_area), "expose_event",
    	        G_CALLBACK(panel_cmd_expose_event), NULL);
		gtk_signal_connect(GTK_OBJECT(panel_cmd->drawing_area), "button_press_event",
		       (GtkSignalFunc) panel_cmd_button_press, NULL);

		gtk_signal_connect(GTK_OBJECT(chart->drawing_area), "button_press_event",
		       (GtkSignalFunc) chart_button_press, NULL);

		// Launch Donkey Thread
		pthread_attr_t ThreadAttr;
		pthread_attr_init(&ThreadAttr);
		//pthread_attr_setdetachstate(&ThreadAttr, PTHREAD_CREATE_DETACHED);
		pthread_create(&(donkey_thread), &ThreadAttr, donkey_update, NULL);
	} else {
		draw_plugin_chart();
	}

	switch_to_display_mode(display_mode);

}

/***** User Config *****/



/*Save values to file*/
static void save_plugin_config(FILE *f) {
	fprintf(f, "%s dl_bandwith_max %d\n", MONITOR_CONFIG_KEYWORD, (int)dl_bandwith_max);
	fprintf(f, "%s ul_bandwith_max %d\n", MONITOR_CONFIG_KEYWORD, (int)ul_bandwith_max);
	fprintf(f, "%s dl_rate_max_paused %d\n", MONITOR_CONFIG_KEYWORD, (int)dl_rate_max_paused);
	fprintf(f, "%s ul_rate_max_paused %d\n", MONITOR_CONFIG_KEYWORD, (int)ul_rate_max_paused);
	fprintf(f, "%s host %s\n", MONITOR_CONFIG_KEYWORD, host);
	fprintf(f, "%s port %d\n", MONITOR_CONFIG_KEYWORD, port);
	fprintf(f, "%s login %s\n", MONITOR_CONFIG_KEYWORD, login);
	fprintf(f, "%s password %s\n", MONITOR_CONFIG_KEYWORD, password);
	fprintf(f, "%s gui_command %s\n", MONITOR_CONFIG_KEYWORD, gui_command);
	fprintf(f, "%s start_core_command %s\n", MONITOR_CONFIG_KEYWORD, start_core_command);
	fprintf(f, "%s show_directory_command %s\n", MONITOR_CONFIG_KEYWORD, show_directory_command);
	fprintf(f, "%s display_mode %d\n", MONITOR_CONFIG_KEYWORD, display_mode);

	gkrellm_save_chartconfig(f, chart_config, MONITOR_CONFIG_KEYWORD, NULL);
}

/*Load values from file*/
static void load_plugin_config(gchar *config_line) {
	gchar	config_keyword[32], config_data[CFG_BUFSIZE];
	gint	n;

	if ((n = sscanf(config_line, "%31s %[^\n]",
			config_keyword, config_data)) < 1)
		return;

	if (n == 1) config_data[0] = '\0';

	if (!strcmp(config_keyword, "dl_bandwith_max"))
		sscanf(config_data, "%f", &dl_bandwith_max);
	else if (!strcmp(config_keyword, "ul_bandwith_max"))
		sscanf(config_data, "%f", &ul_bandwith_max);
	else if (!strcmp(config_keyword, "dl_rate_max_paused"))
		sscanf(config_data, "%f", &dl_rate_max_paused);
	else if (!strcmp(config_keyword, "ul_rate_max_paused"))
		sscanf(config_data, "%f", &ul_rate_max_paused);
	else if (!strcmp(config_keyword, "host"))
		host = g_strdup(config_data);
	else if (!strcmp(config_keyword, "port"))
		sscanf(config_data, "%d", &port);
	else if (!strcmp(config_keyword, "login"))
		login = g_strdup(config_data);
	else if (!strcmp(config_keyword, "password"))
		password = g_strdup(config_data);
	else if (!strcmp(config_keyword, "gui_command"))
		gui_command = g_strdup(config_data);
	else if (!strcmp(config_keyword, "start_core_command"))
		start_core_command = g_strdup(config_data);
	else if (!strcmp(config_keyword, "show_directory_command"))
		show_directory_command = g_strdup(config_data);
	else if (!strcmp(config_keyword, "display_mode"))
		sscanf(config_data, "%d", &display_mode);
	else if (!strcmp(config_keyword, GKRELLM_CHARTCONFIG_KEYWORD))
		gkrellm_load_chartconfig(&chart_config, config_data, 2);
}

/*Get values from config panel*/
static void apply_plugin_config(void) {
    GtkSpinButton *spin;
	gchar *buf;
	gint i;
	gboolean changed = FALSE;

    spin = GTK_SPIN_BUTTON(dl_bandwith_max_widget);
    dl_bandwith_max = gtk_spin_button_get_value_as_int(spin);
    spin = GTK_SPIN_BUTTON(ul_bandwith_max_widget);
    ul_bandwith_max = gtk_spin_button_get_value_as_int(spin);

    spin = GTK_SPIN_BUTTON(dl_rate_max_paused_widget);
    dl_rate_max_paused = gtk_spin_button_get_value_as_int(spin);
    spin = GTK_SPIN_BUTTON(ul_rate_max_paused_widget);
    ul_rate_max_paused = gtk_spin_button_get_value_as_int(spin);

	gkrellm_set_krell_full_scale(dl_krell, dl_bandwith_max, 1);
	gkrellm_set_krell_full_scale(dl_krell_max, dl_bandwith_max, 1);
	gkrellm_set_krell_full_scale(ul_krell, ul_bandwith_max, 1);
	gkrellm_set_krell_full_scale(ul_krell_max, ul_bandwith_max, 1);

	buf = gkrellm_gtk_entry_get_text(&host_widget);
	if (strcmp(buf, host) != 0) {
		host = strdup(buf);
		changed = TRUE;
	}

	buf = gkrellm_gtk_entry_get_text(&port_widget);
	i = atof(buf);
	if (i != port) {
		port = i;
		changed = TRUE;
	}

	buf = gkrellm_gtk_entry_get_text(&login_widget);
	if (strcmp(buf, login) != 0) {
		login = strdup(buf);
		changed = TRUE;
	}

	buf = gkrellm_gtk_entry_get_text(&password_widget);
	if (strcmp(buf, password) != 0) {
		password = strdup(buf);
		changed = TRUE;
	}

	buf = gkrellm_gtk_entry_get_text(&gui_command_widget);
	if (strcmp(buf, gui_command) != 0)
		gui_command = strdup(buf);

	buf = gkrellm_gtk_entry_get_text(&start_core_command_widget);
	if (strcmp(buf, start_core_command) != 0)
		start_core_command = strdup(buf);

	buf = gkrellm_gtk_entry_get_text(&show_directory_command_widget);
	if (strcmp(buf, show_directory_command) != 0)
		show_directory_command = strdup(buf);

	haveToReconnect = changed;
}

static gchar	*plugin_info_text[] =
{
_("<h>Setup Notes\n"),
_("This plugin shows the current download/upload rates of mldonkey.\n\n"),
_("It has three views, switch them with the middle mouse button:\n\n"),
_("<b>* krells:\n"),
_("	Shows the current download rate and upload rate, and their maximum.\n"),
_("	You can change these maximums on-the-fly  by clicking on the krells, or\n"),
_("	 using mouse wheel to increment/decrement.\n"),
_("	The right mouse button opens the plugin configuration window.\n\n"),
_("<b>* chart:\n"),
_("	Shows a chart with upload and download rates.\n"),
_("	The left mouse button shows or hide numeric values.\n"),
_("	The right mouse button opens the chart configuration window.\n\n"),
_("<b>* commands:\n"),
_("	Shows buttons to:\n"),
_("	 - commit downloaded files\n"),
_("	 - Start core (with command specified in options)\n"),
_("	 - Stop core (by sending the kill command to core)\n"),
_("	 - Open GUI (default to \"mlgui\", you can change this in options)\n"),
_("	 - Open File browser on incoming directory (see options, too).\n"),
_("	The right mouse button opens the plugin configuration window.\n\n"),
_("In all views, there is tooltip giving extra informations.\n"),
"\n\n\n",
"Gkremldk - MLDonkey Gkrellm Plugin - version 0.9.7\n",
"2004 Christophe Badoit\n",
"tof-at-tof2k-dot-com\n",
"http://www.tof2k.com\n",
"\n",
"Released under the GNU Public License\n"
};

/*Create the plugin config/help panel */
static void create_plugin_tab(GtkWidget *tab_vbox) {
	GtkWidget	*tabs, *text, *label;
	GtkWidget	*table, *vbox, *vbox1, *vbox2, *hbox;
	gint		i;
	gchar       *buf;

	tabs = gtk_notebook_new();
	gtk_notebook_set_tab_pos(GTK_NOTEBOOK(tabs), GTK_POS_TOP);
	gtk_box_pack_start(GTK_BOX(tab_vbox), tabs, TRUE, TRUE, 0);

	// Plugin Options
	vbox = gkrellm_gtk_framed_notebook_page(tabs, "Options");
	vbox1 = gkrellm_gtk_framed_vbox(vbox,
			_("Your Bandwidth"), 4, FALSE, 0, 2);
	gkrellm_gtk_spin_button(vbox1, &dl_bandwith_max_widget, (gfloat)
			dl_bandwith_max, 5, 2000.0, 1.0, 5.0, 0, 60, NULL, NULL, FALSE,
			_("kB download (i.e. 64)"));
	gkrellm_gtk_spin_button(vbox1, &ul_bandwith_max_widget, (gfloat)
			ul_bandwith_max, 5, 2000.0, 1.0, 5.0, 0, 60, NULL, NULL, FALSE,
			_("kB upload (i.e. 16)"));

	vbox2 = gkrellm_gtk_framed_vbox(vbox,
			_("Commands"), 4, FALSE, 0, 2);
	table = gtk_table_new(5, 2, FALSE /*non-homogeneous*/);
	gtk_table_set_col_spacings(GTK_TABLE(table), 2);
	gtk_box_pack_start(GTK_BOX(vbox2), table, FALSE, FALSE, 2);

	hbox = gtk_hbox_new(TRUE, 0);
	gtk_table_attach(GTK_TABLE(table), hbox, 0, 1, 0, 1,
				GTK_SHRINK, GTK_SHRINK, 0, 0);
	label = gtk_label_new(_("Command to launch GUI : "));
	gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
	gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 4);
	gui_command_widget = gtk_entry_new();
	gtk_entry_set_max_length(GTK_ENTRY(gui_command_widget), 255);
	gtk_table_attach_defaults(GTK_TABLE(table), gui_command_widget,
				1, 2, 0, 1);
	gtk_entry_set_text(GTK_ENTRY(gui_command_widget), gui_command);

	hbox = gtk_hbox_new(TRUE, 0);
	gtk_table_attach(GTK_TABLE(table), hbox, 0, 1, 1, 2,
				GTK_SHRINK, GTK_SHRINK, 0, 0);
	label = gtk_label_new(_("Command to start core : "));
	gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
	gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 4);
	start_core_command_widget = gtk_entry_new();
	gtk_entry_set_max_length(GTK_ENTRY(start_core_command_widget), 255);
	gtk_table_attach_defaults(GTK_TABLE(table), start_core_command_widget,
				1, 2, 1, 2);
	gtk_entry_set_text(GTK_ENTRY(start_core_command_widget),
			start_core_command);

	hbox = gtk_hbox_new(TRUE, 0);
	gtk_table_attach(GTK_TABLE(table), hbox, 0, 1, 2, 3,
				GTK_SHRINK, GTK_SHRINK, 0, 0);
	label = gtk_label_new(_("Command to show incoming files : "));
	gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
	gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 4);
	show_directory_command_widget = gtk_entry_new();
	gtk_entry_set_max_length(GTK_ENTRY(show_directory_command_widget), 255);
	gtk_table_attach_defaults(GTK_TABLE(table), show_directory_command_widget,
				1, 2, 2, 3);
	gtk_entry_set_text(GTK_ENTRY(show_directory_command_widget),
			show_directory_command);

	// Server Options
	
	vbox = gkrellm_gtk_framed_notebook_page(tabs, _("Server Setup"));

	vbox1 = gkrellm_gtk_framed_vbox_end(vbox, NULL, 4, FALSE, 0, 2);
	table = gtk_table_new(5, 2, FALSE /*non-homogeneous*/);
	gtk_table_set_col_spacings(GTK_TABLE(table), 2);
	gtk_box_pack_start(GTK_BOX(vbox1), table, FALSE, FALSE, 2);

	hbox = gtk_hbox_new(TRUE, 0);
	gtk_table_attach(GTK_TABLE(table), hbox, 0, 1, 0, 1,
				GTK_SHRINK, GTK_SHRINK, 0, 0);
	label = gtk_label_new(_("Server host name : "));
	gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
	gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 4);
	host_widget = gtk_entry_new();
	gtk_entry_set_max_length(GTK_ENTRY(host_widget), 255);
	gtk_table_attach_defaults(GTK_TABLE(table), host_widget,
				1, 2, 0, 1);
	gtk_entry_set_text(GTK_ENTRY(host_widget),
				host);

	hbox = gtk_hbox_new(TRUE, 0);
	gtk_table_attach(GTK_TABLE(table), hbox, 0, 1, 1, 2,
				GTK_SHRINK, GTK_SHRINK, 0, 0);
	label = gtk_label_new(_("Server port number : "));
	gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
	gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 4);
	port_widget = gtk_entry_new();
	gtk_entry_set_max_length(GTK_ENTRY(port_widget), 6);
	gtk_table_attach_defaults(GTK_TABLE(table), port_widget, 1, 2, 1, 2);
	buf = g_strdup_printf("%d", port);
	gtk_entry_set_text(GTK_ENTRY(port_widget), buf);
	free(buf);

	hbox = gtk_hbox_new(TRUE, 0);
	gtk_table_attach(GTK_TABLE(table), hbox, 0, 1, 2, 3,
				GTK_SHRINK, GTK_SHRINK, 0, 0);
	label = gtk_label_new(_("Server login (user name) : "));
	gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
	gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 4);
	login_widget = gtk_entry_new();
	gtk_entry_set_max_length(GTK_ENTRY(login_widget), 20);
	gtk_table_attach_defaults(GTK_TABLE(table), login_widget, 1, 2, 2, 3);
	gtk_entry_set_text(GTK_ENTRY(login_widget), login);

	hbox = gtk_hbox_new(TRUE, 0);
	gtk_table_attach(GTK_TABLE(table), hbox, 0, 1, 3, 4,
				GTK_SHRINK, GTK_SHRINK, 0, 0);
	label = gtk_label_new(_("Server password : "));
	gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
	gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 4);
	password_widget = gtk_entry_new();
	gtk_entry_set_max_length(GTK_ENTRY(password_widget), 20);
	gtk_table_attach_defaults(GTK_TABLE(table), password_widget, 1, 2, 3, 4);
	gtk_entry_set_text(GTK_ENTRY(password_widget), password);

	// Help/About
	vbox = gkrellm_gtk_framed_notebook_page(tabs, _("Info"));
	text = gkrellm_gtk_scrolled_text_view(vbox, NULL,
			GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	for (i = 0; i < sizeof(plugin_info_text)/sizeof(gchar *); ++i)
		gkrellm_gtk_text_view_append(text, plugin_info_text[i]);
	}


/*Monitor Infos*/
static GkrellmMonitor	plugin_mon	=
	{
	CONFIG_NAME,        /* Title for config clist.   */
	0,                  /* Id,  0 if a plugin       */
	create_plugin,      /* The create function      */
	update_plugin,      /* The update function      */
	create_plugin_tab,      /* The create_plugin_tab() config function */
	apply_plugin_config,    /* The apply_plugin_config() function      */

	save_plugin_config,     /* The save_plugin_config() function  */
	load_plugin_config,     /* The load_plugin_config() function  */
	MONITOR_CONFIG_KEYWORD, /* config keyword                     */

	NULL,               /* Undefined 2	*/
	NULL,               /* Undefined 1	*/
	NULL,               /* private	*/

	MON_MAIL,           /* Insert plugin before this monitor			*/

	NULL,               /* Handle if a plugin, filled in by GKrellM     */
	NULL                /* path if a plugin, filled in by GKrellM       */
	};

/*Initialize Plugin*/
GkrellmMonitor * gkrellm_init_plugin(void)
{
	g_ticks = gkrellm_ticks();
	style_id = gkrellm_add_meter_style(&plugin_mon, STYLE_NAME);
	monitor = &plugin_mon;

	return &plugin_mon;
}

